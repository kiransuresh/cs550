/* @author Kiran S <ks@hawk.iit.edu> */
#include "connection.hpp"

/** 
 * Constructor for class, initilize socket for this connection
 * 
 * @param io_service reference to io_service
 * 
 * @return nothing
 */

using namespace std;
using namespace boost::filesystem;
bool readFile(std::string &fileName, std::string &fileContent)
{
    boost::filesystem::ifstream fIn;

    // Obj fIn can has as argument string fname:
    // fIn.open(fname,std::ios::in);
    // either obj boost::filesystem::path:
    // fIn.open(path(fname),std::ios::in);
    // But for comparibility with std::ifstream I use:
	
    fileName = "./" + fileName;
    fIn.open(fileName.c_str(),std::ios::in);

    if (!fIn) {
        cerr << "string load_data_in_str(string filename)" << endl;
        cerr << "Error reading the file: " << fileName << endl;
        getchar();
	return 1;
    }

    string l;

    stringstream ss;

    ss << fIn.rdbuf();
    
    fileContent =  ss.str();
    return 0;	
}
peerRegistry connection::p_registry;

connection::connection(ba::io_service& io_service, hide_me) :
	io_service_(io_service), socket_(io_service) {
	}


/** 
 * Perform all input/output operations in sync mode
 * 
 */


void connection::indexServiceRun() {
	try {
		int req;

		// read request type
		ba::read(
				socket_,
				ba::buffer( &req, sizeof(req) )
			);
		//dispatch based on request type
		if (REGISTER_REQ == req)
			processRegistration();
		else if (FILE_REQ == req)
			processFileRequest();
		else if (REGISTRY_REQ == req)
			processSystemRegistry();


	} catch(std::exception& x) {
		std::cerr << "Exception: " << x.what() << std::endl;
	}
}

//process peer registration at index server
void connection::processRegistration()
{
	try {
		std::cout << "processing registration from peer\n";
		size_t header;

		// read header
		ba::read(
				socket_,
				ba::buffer( &header, sizeof(header) )
			);
//		std::cout << "body is " << header << " bytes" << std::endl;

		// read body
		boost::asio::streambuf buf;
		const size_t rc = ba::read(
				socket_,
				buf.prepare( header )
				);
		buf.commit( header );
//		std::cout << "read " << rc << " bytes" << std::endl;

		// deserialize
		std::istream is( &buf );
		boost::archive::text_iarchive ar( is );
		PeerRegistryMessage msg;
		ar & msg;

		//Add to registry
		p_registry[msg._pid] = msg._fileList;
		/*
		std::cout << "pid is " << msg._pid << std::endl;
		std::cout << "filesize is " << msg._fileList.size() << std::endl;
		std::cout << "pid is " << msg._pid << std::endl;
		std::cout << "filesize is " << p_registry[msg._pid].size() << std::endl;
		
		for (int i =0; i < msg._fileList.size(); ++i)
		{
			std::cout << "File: " << p_registry[msg._pid][i] << "\t" << std::endl;
		}

		*/
		std::cout << "\nReceived New Peer! System directory contains\n";
		peerRegistry::iterator it;

       		for (it = p_registry.begin(); it !=p_registry.end(); it++)
       		{
       		 	std::cout << "Peer id: " << it->first << std::endl;
			std::cout << "File List:\n";

       		 	for (int i =0; i < it->second.size(); ++i)
       		         {
       		                 std::cout << p_registry[it->first][i] << "\t";
       		         }
       		 	std::cout << std::endl;
       		}
	} catch(std::exception& x) {
		std::cerr << "Exception: " << x.what() << std::endl;
	}
}

void connection::processFileRequest()
{
	try {
		std::cout << "processing fileRequest\n";

		//read file request

		size_t rheader;

		// read header
		ba::read(
				socket_,
				ba::buffer( &rheader, sizeof(rheader) )
			);
		std::cout << "body is " << rheader << " bytes" << std::endl;

		// read body
		boost::asio::streambuf buf;
		const size_t rc = ba::read(
				socket_,
				buf.prepare( rheader )
				);
		buf.commit( rheader );
		std::cout << "read " << rc << " bytes" << std::endl;

		// deserialize
		std::istream is( &buf );
		boost::archive::text_iarchive ar( is );
		FileReqMessage msg;
		ar & msg;

		std::cout << "Requested File: " << msg._fileName;
		std::cout << msg._fileContents;


		if (readFile(msg._fileName, msg._fileContents))
   		{
			msg._fileName = "fileNotFound";
 			msg._fileContents = "fileNotFound";
		}

    		boost::asio::streambuf buf2;
    		std::ostream os2( &buf2 );
    		boost::archive::text_oarchive ar2( os2 );
    		ar2 & msg;

		int req = FILE_REP;
    		const size_t wheader = buf.size();
    		std::cout << "buffer size " << wheader << " bytes" << std::endl;

    		// send header and buffer using scatter
    		std::vector<boost::asio::const_buffer> buffers;
		buffers.push_back( boost::asio::buffer(&req, sizeof(req)) );
    		buffers.push_back( boost::asio::buffer(&wheader, sizeof(wheader)) ); //send header containing size
    		buffers.push_back( buf2.data() ); //send actual data
    		const size_t rc2 = boost::asio::write(
    		        socket_,
    		        buffers
    		        );
    		std::cout << "wrote " << rc2 << " bytes" << std::endl;;


	} catch(std::exception& x) {
		std::cerr << "Exception: " << x.what() << std::endl;
	}
}

void connection::processSystemRegistry()
{

}

void connection::peerServiceRun() {
	try {
		// read header
		size_t header;
		ba::read(
				socket_,
				ba::buffer( &header, sizeof(header) )
			);
		std::cout << "body is " << header << " bytes" << std::endl;

		// read body
		boost::asio::streambuf buf;
		const size_t rc = ba::read(
				socket_,
				buf.prepare( header )
				);
		buf.commit( header );
		std::cout << "read " << rc << " bytes" << std::endl;

		// deserialize
		std::istream is( &buf );
		boost::archive::text_iarchive ar( is );
		FileReqMessage msg;
		ar & msg;

		std::cout << msg._fileName << std::endl;
		std::cout << msg._fileContents << std::endl;


	} catch(std::exception& x) {
		//		std::cerr << "Exception: " << x.what() << std::endl;
	}
}
