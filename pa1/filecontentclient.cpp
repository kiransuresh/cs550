/* @author Kiran S <ks@hawk.iit.edu> */

#include "Message.h"

#include <boost/archive/text_oarchive.hpp>

#include <boost/asio.hpp>
#include "fileList.hpp"

int
main()
{
    FileReqMessage msg;
  /*  msg._fileName = "hello";
    msg._fileContents = "world";
  */
    std::vector<std::string> files;
    int fileCount;
    if (!fileList(fileCount,files,"./")   )
	{
	std::string inputFile;
	std::string inputFileContents;

	std::cout << "Choose file: " ;
	std::cin >> inputFile;

	readFile(inputFile, inputFileContents);
   
	msg._fileName = inputFile;
 	msg._fileContents = inputFileContents;

	}

    boost::asio::streambuf buf;
    std::ostream os( &buf );
    boost::archive::text_oarchive ar( os );
    ar & msg;

    boost::asio::io_service io_service;
    boost::asio::ip::tcp::socket socket( io_service );
    const short port = 10001;
    socket.connect(
            boost::asio::ip::tcp::endpoint(
                boost::asio::ip::address::from_string( "127.0.0.1" ),
                port
                )
            );

    const size_t header = buf.size();
    std::cout << "buffer size " << header << " bytes" << std::endl;

    // send header and buffer using scatter
    std::vector<boost::asio::const_buffer> buffers;
    buffers.push_back( boost::asio::buffer(&header, sizeof(header)) ); //send header containing size
    buffers.push_back( buf.data() ); //send actual data
    const size_t rc = boost::asio::write(
            socket,
            buffers
            );
    std::cout << "wrote " << rc << " bytes" << std::endl;;
}
