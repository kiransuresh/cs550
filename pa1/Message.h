/* @author Kiran S <ks@hawk.iit.edu> */
#ifndef MESSAGE_H
#define MESSAGE_H

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/list.hpp>
#include <boost/serialization/vector.hpp>

#include <string>
#include <vector>
#include <map>

//Request Messages
#define REGISTER_REQ 1
#define FILE_REQ 2
#define REGISTRY_REQ 3

//Response Messages
#define FILE_REP 10
#define REGISTRY_REP 11

struct FileReqMessage
{
    std::string _fileName;
    std::string _fileContents;

    template <class Archive>
    void serialize(
            Archive& ar,
            unsigned int version
            )
    {
        ar & _fileName;
        ar & _fileContents;
    }
};

struct FileListMessage
{
    std::map<int,std::vector<std::string>> _directory;

    template <class Archive>
    void serialize(
            Archive& ar,
            unsigned int version
            )
    {
        ar & _directory;
    }
};

struct PeerRegistryMessage
{
	int _pid;
	std::vector<std::string> _fileList;

	template <class Archive>
	void serialize(
			Archive& ar,
			unsigned int version
			)
	{
		ar & _pid;
		ar & _fileList;
	}

};
#endif
