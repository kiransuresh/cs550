/**
 * @file   peerService.cpp
 * @author Kiran S <ks@hawk.iit.edu>
 * 
 * @brief Implementation of server with implementation of thread per
 * connection strategy.  Working threads use sync mode for input/output, but server thread
 * work in async mode.
 * 
 * 
 */

#include "connection.hpp"
#include <set>
#include <thread>

using namespace std;
using namespace boost::filesystem;

bool fileList(int &count, std::vector<std::string> &files, std::string dirPath)
{
	if (dirPath == "")
	{
		cout << "Path Empty\n";
		return 1;
	}

	path p (dirPath.c_str());   // p reads clearer than argv[1] in the following code

	try
	{
		if (exists(p))    // does p actually exist?
		{
			if (is_regular_file(p))        // is p a regular file?
				cout << p << " size is " << file_size(p) << '\n';

			else if (is_directory(p))      // is p a directory?
			{
				cout << p << " is a directory containing:\n";

				typedef vector<path> vec;             // store paths,
				vec v;                                // so we can sort them later

				copy(directory_iterator(p), directory_iterator(), back_inserter(v));

				sort(v.begin(), v.end());             // sort, since directory iteration
				// is not ordered on some file systems

				for (vec::const_iterator it(v.begin()), it_end(v.end()); it != it_end; ++it)
				{
					cout << "   " << it->filename() << '\n';
					files.push_back(it->filename().c_str());
				}
			}
			else
				cout << p << " exists, but is neither a regular file nor a directory\n";
		}
		else
			cout << p << " does not exist\n";
	}

	catch (const filesystem_error& ex)
	{
		cout << ex.what() << '\n';
	}

	return 0;
}
/**
 * Peer Service class
 * 
 */
class peer : private boost::noncopyable {
	public:
		peer(ba::io_service& io_service, int port=10001);
		void startConsole();

	private:
		void handle_accept(const boost::system::error_code& e);

		ba::io_service& io_service_;         /**< reference to io_service */
		ba::ip::tcp::acceptor acceptor_;     /**< object, that accepts new connections */
		connection::pointer new_connection_; /**< pointer to connection, that will proceed next */
};

/** 
 * Initialize all needed data
 * 
 * @param io_service reference to io_service
 * @param port port to listen on, by default - 10001
 */
peer::peer(ba::io_service& io_service,int port)
	: io_service_(io_service),
	acceptor_(io_service_, ba::ip::tcp::endpoint(ba::ip::tcp::v4(), port)),
	new_connection_(connection::create(io_service_)) {
		// start acceptor in async mode
		acceptor_.async_accept(new_connection_->socket(),
				boost::bind(&peer::handle_accept, this,
					ba::placeholders::error));
	}

/** 
 * Start console service
 * 
 */
void peer::startConsole()
	/** 
	 * Run when new connection is accepted
	 * 
	 */
{
	std::cout << "Started Console\n";
	boost::this_thread::sleep(boost::posix_time::seconds(1));
}

void peer::handle_accept(const boost::system::error_code& e) {
	if (!e) {
		// run connection in new thread
		boost::thread t(boost::bind(&connection::peerServiceRun, new_connection_));
		// create next connection, that will accepted
		new_connection_=connection::create(io_service_);
		// start new accept operation
		acceptor_.async_accept(new_connection_->socket(),
				boost::bind(&peer::handle_accept, this,
					ba::placeholders::error));
	}
}

/****** Console Services **************/

boost::asio::io_service my_io_service;
boost::asio::posix::stream_descriptor in(my_io_service, ::dup(STDIN_FILENO));
boost::asio::deadline_timer timer(my_io_service);

class consoleServices
{
	public:
		consoleServices(ba::io_service& io_service, int sport, int port=10001):io_service_(io_service),_sport(sport),_port(port) {}

		void registerWithIndexServer()
		{
			std::cout << "Connecting to Index Server at port: " << _port << std::endl;
			boost::asio::ip::tcp::socket socket( io_service_ );
			socket.connect(
					boost::asio::ip::tcp::endpoint(
						boost::asio::ip::address::from_string( "127.0.0.1" ),
						_port
						)
				      );

			PeerRegistryMessage msg;

			std::vector<std::string> files;
			int fileCount;
			if (!fileList(fileCount,files,"./")   )
			{

				//msg._pid = getpid();
				msg._pid = _sport;
				msg._fileList = files;

			}

			//seriliaze
			boost::asio::streambuf buf;
			std::ostream os( &buf );
			boost::archive::text_oarchive ar( os );
			ar & msg;

			int req = REGISTER_REQ;
			const size_t header = buf.size();
			std::cout << "buffer size " << header << " bytes" << std::endl;

			// send header and buffer using scatter
			std::vector<boost::asio::const_buffer> buffers;
			buffers.push_back( boost::asio::buffer(&req, sizeof(req)) );
			buffers.push_back( boost::asio::buffer(&header, sizeof(header)) );
			buffers.push_back( buf.data() );
			const size_t rc = boost::asio::write(
					socket,
					buffers
					);
			std::cout << "wrote " << rc << " bytes" << std::endl;;

			boost::system::error_code ec;
			socket.shutdown( boost::asio::ip::tcp::socket::shutdown_both, ec );
			socket.close( ec );

		}

		void requestFileFromPeer(int peerId, std::string filename)
		{
			std::cout << "Connecting to Peer at port: " << peerId << std::endl;
			boost::asio::ip::tcp::socket socket( io_service_ );
			socket.connect(
					boost::asio::ip::tcp::endpoint(
						boost::asio::ip::address::from_string( "127.0.0.1" ),
						peerId
						)
				      );

			FileReqMessage msg;
			msg._fileName = filename;


			//seriliaze
			boost::asio::streambuf buf;
			std::ostream os( &buf );
			boost::archive::text_oarchive ar( os );
			ar & msg;

			int req = FILE_REQ;
			const size_t header = buf.size();
			std::cout << "buffer size " << header << " bytes" << std::endl;

			// send header and buffer using scatter
			std::vector<boost::asio::const_buffer> buffers;
			buffers.push_back( boost::asio::buffer(&req, sizeof(req)) );
			buffers.push_back( boost::asio::buffer(&header, sizeof(header)) );
			buffers.push_back( buf.data() );
			const size_t rc = boost::asio::write(
					socket,
					buffers
					);
			std::cout << "wrote " << rc << " bytes" << std::endl;;

			boost::system::error_code ec;
			socket.shutdown( boost::asio::ip::tcp::socket::shutdown_both, ec );
			socket.close( ec );

		}

		// perform reading loop
		void reading_loop()
		{
			std::cout << "Menu: \n";
			std::cout << "1. View Registry\n";
			std::cout << "2. Request File\n";
			static boost::asio::streambuf buffer; // todo some encapsulation :)


			async_read_until(in, buffer, '\n', [&](boost::system::error_code ec, size_t bytes_transferred) {
					if (!ec)
					{
					std::string line;
					std::istream is(&buffer);
					if (std::getline(is, line) && line == "exit")
					ec = boost::asio::error::operation_aborted;
					else if (line == "1")
					std::cout << "Processing Registry request\n";

					else if (line == "2")
					{
						buffer.consume(line.size());
						int peerId;
						std::string filename;
						std::cout << "Processing File request\n";
						std::cout << "Enter file name: " ;
						std::cout << "Enter Peer: " ;
						requestFileFromPeer(peerId, filename);
					}
					else 
					std::cout << "Invalid input. Try again...\n";
					reading_loop(); // continue
					}

					if (ec)
					{
					std::cerr << "Exiting due to: " << ec.message() << "\n";
					// in this case, we don't want to wait until the timeout expires
					timer.cancel();
					}
			});
		}

		void operator()()
		{
			registerWithIndexServer();
			reading_loop();
			my_io_service.run();
		}

	private:
		ba::io_service& io_service_;         /**< reference to io_service */
		int _sport;
		int _port;

};


/****************************************/
/** 
 * Main routine
 * 
 * @param argc number of arguments
 * @param argv pointers to arguments
 * 
 * @return error code
 */
int main(int argc, char** argv) {
	try {
		int port=1234, indexport = 10001;
		// read port number from command line, if provided
		if(argc > 1)
			port=boost::lexical_cast<int>(argv[1]);
		//ba::io_service io_service;
		boost::shared_ptr< boost::asio::io_service > io_service(
				new boost::asio::io_service
				);
		// construct new peer object
		peer s(*io_service,port);

		consoleServices x(*io_service,port,indexport);
		//consoleServices x;
		boost::thread t(x);
		// run io_service object, that perform all dispatch operations
		//io_service.run();
		io_service->run();

		std::cout << "Exiting";
	} catch (std::exception& e) {
		std::cerr << e.what() << std::endl;
	}

	return 0;
}


