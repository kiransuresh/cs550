/* @author Kiran S <ks@hawk.iit.edu> */

#include "Message.h"

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <boost/archive/binary_oarchive.hpp> 
#include <boost/archive/binary_iarchive.hpp>

#include <boost/asio.hpp>
#include "fileList.hpp"

#include <sys/types.h>
#include <unistd.h>

int main()
{
	PeerRegistryMessage msg;

	std::vector<std::string> files;
	int fileCount;
	if (!fileList(fileCount,files,"./")   )
	{

		msg._pid = getpid();
		msg._fileList = files;

	}
	
	//seriliaze
	boost::asio::streambuf buf;
	std::ostream os( &buf );
	boost::archive::text_oarchive ar( os );
	ar & msg;


/*
	 // deserialize
	std::istream is( &buf );
	boost::archive::text_iarchive ar2( is );
	PeerRegistryMessage msg2;
	ar2 & msg2;
	
*/
	std::cout << "Returned files in vec for " << msg._pid << std::endl ;

        for (std::vector<std::string>::const_iterator it(msg._fileList.begin()), it_end(msg._fileList.end()); it != it_end; ++it)
        {
		std::cout << *it;
        }
	boost::asio::io_service io_service;
	boost::asio::ip::tcp::socket socket( io_service );
	const short port = 10001;
	socket.connect(
			boost::asio::ip::tcp::endpoint(
				boost::asio::ip::address::from_string( "127.0.0.1" ),
				port
				)
		      );

	const size_t header = buf.size();
	std::cout << "buffer size " << header << " bytes" << std::endl;

	// send header and buffer using scatter
	std::vector<boost::asio::const_buffer> buffers;
	buffers.push_back( boost::asio::buffer(&header, sizeof(header)) ); //send header containing size
	buffers.push_back( buf.data() ); //send actual data
	const size_t rc = boost::asio::write(
			socket,
			buffers
			);
	std::cout << "wrote " << rc << " bytes" << std::endl;;
}
