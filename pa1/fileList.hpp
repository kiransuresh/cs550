/* @author Kiran S <ks@hawk.iit.edu> */
/*
#include <iostream>
#include <iterator>
#include <vector>
#include <algorithm>
#include <boost/filesystem.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/fstream.hpp>
*/

#include "common.h"
using namespace std;
using namespace boost::filesystem;

bool fileList(int &count, std::vector<std::string> &files, std::string dirPath)
{
  if (dirPath == "")
  {
    cout << "Path Empty\n";
    return 1;
  }

  path p (dirPath.c_str());   // p reads clearer than argv[1] in the following code

  try
  {
    if (exists(p))    // does p actually exist?
    {
      if (is_regular_file(p))        // is p a regular file?
        cout << p << " size is " << file_size(p) << '\n';

      else if (is_directory(p))      // is p a directory?
      {
        cout << p << " is a directory containing:\n";

        typedef vector<path> vec;             // store paths,
        vec v;                                // so we can sort them later

        copy(directory_iterator(p), directory_iterator(), back_inserter(v));

        sort(v.begin(), v.end());             // sort, since directory iteration
                                              // is not ordered on some file systems

        for (vec::const_iterator it(v.begin()), it_end(v.end()); it != it_end; ++it)
        {
          cout << "   " << it->filename() << '\n';
	  files.push_back(it->filename().c_str());
        }
      }
      else
        cout << p << " exists, but is neither a regular file nor a directory\n";
    }
    else
      cout << p << " does not exist\n";
  }

  catch (const filesystem_error& ex)
  {
    cout << ex.what() << '\n';
  }

  return 0;
}


bool readFile(std::string &fileName, std::string &fileContent)
{
    boost::filesystem::ifstream fIn;

    // Obj fIn can has as argument string fname:
    // fIn.open(fname,std::ios::in);
    // either obj boost::filesystem::path:
    // fIn.open(path(fname),std::ios::in);
    // But for comparibility with std::ifstream I use:
	
    fileName = "./" + fileName;
    fIn.open(fileName.c_str(),std::ios::in);

    if (!fIn) {
        cerr << "string load_data_in_str(string filename)" << endl;
        cerr << "Error reading the file: " << fileName << endl;
        getchar();
	return 1;
    }

    string l;

    stringstream ss;

    ss << fIn.rdbuf();
    
    fileContent =  ss.str();
    return 0;	
}
